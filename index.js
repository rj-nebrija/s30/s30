/*
	Mini Activity:
		Create expressapi port 4000
		create a new route with endpoint /hello and method GET
			-should be able to respond with "Hello World."

*/

const express = require('express')
const mongoose = require('mongoose')

const app = express()

mongoose.connect('mongodb+srv://rjnebrija:superman123@cluster0.flsml.mongodb.net/B157_to-do?retryWrites=true&w=majority', 
					{
						useNewUrlParser : true,
						useUnifiedTopology: true
					})

let db = mongoose.connection

db.on("error", console.error.bind(console, "Connection error"))

db.on('open', () => console.log("We're connected to the cloud database"))

//SCHEMA
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

const userSchema = new mongoose.Schema({
	username : {
		type : String, 
		default : 'admin'
	},
	password : {
		type : String, 
		default : 'password'
	}
})


//MODEL
const Task = mongoose.model("Task", taskSchema)
const User = mongoose.model("User", userSchema)

app.use(express.json())
app.use(express.urlencoded({extended: true}))


app.post('/tasks', (req, res) => {

	Task.findOne({name : req.body.name}, (err,result) => {
		if(result != null && result.name == req.body.name) {
			return res.send("Duplicate Task Found")
		} else {
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask)=> {
				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send("New Task created!")
				}
			})
		}
	})
})

//GET retriee all documents

app.get('/tasks', (req,res)=> {
	Task.find({}, (err,result)=> {
		if(err) {
			console.log(err)
		} else {
			return res.status(200).json({data : result})
		}
	})



})


app.get('/hello', (req,res)=> {
	res.send('Hello World.')
})



app.post('/signup', (req, res) => {
	User.findOne({username : req.body.username}, (err, result) => {
		console.log(result)
		if(result != null && result.username == req.body.username) {
			return res.send("Duplicate user found")
		} else {
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})
		
			newUser.save((saveErr, savedUser) => {
				if(saveErr) {
					return res.send("Error saving")
				} else {
					return res.send(`User ${savedUser.username} saved to database!`)
				}
			})
		}
	})
} )



app.listen(4000, ()=> console.log("Listening at port 4000"))